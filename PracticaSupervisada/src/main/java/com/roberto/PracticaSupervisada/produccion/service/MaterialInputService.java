/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.MaterialInputRepository;
import com.roberto.PracticaSupervisada.produccion.dao.MaterialRepository;
import com.roberto.PracticaSupervisada.produccion.model.Material;
import com.roberto.PracticaSupervisada.produccion.model.MaterialInput;
import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class MaterialInputService {
    @Autowired
    private MaterialInputRepository matInRepo;
 
    @Autowired
    private MaterialService matServ;
    //-----------------------
    
    //save a input
    public Boolean addMaterialInput(Long idMaterial,int bags){
        Material material=matServ.getMaterial(idMaterial);
        if(material==null)
            return false;
        
        MaterialInput newMatInput=new MaterialInput(bags,material,
                Calendar.getInstance().getTime(),true);
        
        newMatInput.setCreationDate(OffsetDateTime.now());
        
       float numOfKilToAdd=bags * 25f;
       
        if(!matServ.addMaterial(idMaterial, numOfKilToAdd))
             return false;
            
        matInRepo.save(newMatInput);
          
        return true;         
    }

    public Boolean reduceMaterialInput(Long idMaterial, int numBags) {
        Material material=matServ.getMaterial(idMaterial);
        if(material==null)
            return null;
        
        MaterialInput newMatInput=new MaterialInput(numBags, material,
                Calendar.getInstance().getTime(),false);
        
        newMatInput.setCreationDate(OffsetDateTime.now());
        
        
        float numMat=newMatInput.getNumOfBags() * 25f;
        
        
        if(!matServ.reduceMaterial(idMaterial,numMat))
            return false;
            
        matInRepo.save(newMatInput);
        
        return true;        
    }
    
    public List<MaterialInput> getAllMaterialInput() {
        return matInRepo.findByOrderByCreationDateDesc();
        //return matInRepo.findAll();
    }

    public List<MaterialInput> getMaterialInput(int page, int mount) {
        //Pageable pagina=PageRequest.of(page,mount,Sort.by("creationDate").descending());
        Pageable pagina=PageRequest.of(page,mount);
        
        List<MaterialInput> result=matInRepo.findByOrderByCreationDateDesc(pagina);
        
        return result;
    }
   
    
}
