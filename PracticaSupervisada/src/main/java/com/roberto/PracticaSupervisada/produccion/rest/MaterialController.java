/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.Material;
import com.roberto.PracticaSupervisada.produccion.service.MaterialService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class MaterialController {
    @Autowired
    private MaterialService materialServ;
    //-------------------
    
    @GetMapping("/Materials")
    public ResponseEntity getAllMaterials(){
        List<Material> lst=materialServ.getAllMaterial();
        return new ResponseEntity(lst, HttpStatus.OK);
    }
    
    @GetMapping("/Materials/{idMaterial}")
    public ResponseEntity getMaterial(@PathVariable("idMaterial")Long idMaterial){
        Material material=materialServ.getMaterial(idMaterial);
        HttpStatus status=this.evalStatus(material);
        
        return new ResponseEntity(material, status);
    }
    
    @DeleteMapping("/Materials/{idMaterial}")
    public ResponseEntity deleteMaterial(@PathVariable("idMaterial")Long idMaterial){
       Boolean result=materialServ.deleteMaterial(idMaterial);
        HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
     @PostMapping("/Materials")
     public ResponseEntity saveMaterial(@RequestBody Material material){
         Boolean result=materialServ.saveMaterial(material);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
     }
     
     @PostMapping("/Materials/{idMaterial}/Add/{mount}")
     public ResponseEntity addMaterial(@PathVariable("idMaterial")Long idMaterial,
                        @PathVariable("mount")float mount){
         
        HttpStatus status;
        if(materialServ.addMaterial(idMaterial, mount))
            status=HttpStatus.OK;
            else
            status=HttpStatus.NOT_FOUND;
        
        return new ResponseEntity(materialServ.getMaterial(idMaterial), status);
     }
     
     @PostMapping("/Materials/{idMaterial}/Reduce/{mount}")
     public ResponseEntity reduceMaterial(@PathVariable("idMaterial")Long idMaterial,
                        @PathVariable("mount")float mount){
         
        HttpStatus status;
        if(materialServ.reduceMaterial(idMaterial, mount))
            status=HttpStatus.OK;
            else
            status=HttpStatus.NOT_FOUND;
        
        return new ResponseEntity(materialServ.getMaterial(idMaterial), status);
     }
     
     @PutMapping("/Materials")
     public ResponseEntity updateMaterial(@RequestBody Material material){
         Boolean result=materialServ.updateMaterial(material);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
     }
    //*******************************************************
    private HttpStatus evalStatus(Material material){
        HttpStatus result;
        if(material!=null)
            result=HttpStatus.OK;
        else
            result=HttpStatus.NOT_FOUND;
        return result;
    }
}//endController
