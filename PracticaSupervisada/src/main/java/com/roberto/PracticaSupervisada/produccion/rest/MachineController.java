/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.Machine;
import com.roberto.PracticaSupervisada.produccion.service.MachineService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class MachineController {
    @Autowired
    private MachineService machineServ;
    //-----------------------
    @GetMapping("/Machines")
    public ResponseEntity getAllMachines(){
        List<Machine> list=machineServ.getAllMachines();
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @GetMapping("/Machines/{idMachine}")
    public ResponseEntity getMachine(@PathVariable("idMachine")Long idMachine){
        Machine machine=machineServ.getMachine(idMachine);
        
        return new ResponseEntity(machine, evalStatus(machine));
    }
    
    @PostMapping("/Machines")
    public ResponseEntity saveMachine(@RequestBody Machine machine){
        Boolean result = machineServ.saveMachine(machine);
        HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @PutMapping("/Machines")
    public ResponseEntity updateMachine(@RequestBody Machine machine){
         Boolean result=machineServ.updateMachine(machine);
        HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @DeleteMapping("/Machines/{idMachine}")
    public ResponseEntity deleteMachine(@PathVariable("idMachine")Long idMachine){
        Boolean result=machineServ.deleteMachine(idMachine);
       HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
     //*******************************************************
    private HttpStatus evalStatus(Machine machine){
        HttpStatus result;
        if(machine!=null)
            result=HttpStatus.OK;
        else
            result=HttpStatus.NOT_FOUND;
        return result;
    }
}//endcontroller
