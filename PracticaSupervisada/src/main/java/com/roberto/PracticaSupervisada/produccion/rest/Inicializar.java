/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;


import com.roberto.PracticaSupervisada.produccion.dao.AppUserRepository;
import com.roberto.PracticaSupervisada.produccion.dao.EntryLevelRepository;
import com.roberto.PracticaSupervisada.produccion.dao.StatusPORepository;

import com.roberto.PracticaSupervisada.produccion.model.AppUser;
import com.roberto.PracticaSupervisada.produccion.model.EntryLevel;
import com.roberto.PracticaSupervisada.produccion.model.StatusPO;
import java.time.OffsetDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class Inicializar {
    @Autowired
    private StatusPORepository stRepo;
    @Autowired
    private EntryLevelRepository elRepo;
    @Autowired
    private AppUserRepository auRepo;
    
    //--------------
    @GetMapping("/Inicializar")
    public ResponseEntity inicializar(){
        //statusop
        long test=stRepo.count();
        if(test==0){
            StatusPO stp=new StatusPO();
            stp.setId(1L);
            stp.setCreationDate(OffsetDateTime.now());
            stp.setName("PRODUCCION");

            StatusPO stt=new StatusPO();
            stt.setId(2L);
            stt.setCreationDate(OffsetDateTime.now());
            stt.setName("TERMINADO");

            StatusPO stc=new StatusPO();
            stc.setId(3L);
            stc.setCreationDate(OffsetDateTime.now());
            stc.setName("CANCELADO");
            
            StatusPO ste=new StatusPO();
            ste.setId(4L);
            ste.setCreationDate(OffsetDateTime.now());
            ste.setName("ENTREGADO");
            
           stRepo.save(stp);
           stRepo.save(stt);
           stRepo.save(stc);
           stRepo.save(ste);
           
        }
         //EntryLevel
         long count2=elRepo.count();
         if(count2==0){
             EntryLevel elUno=new EntryLevel();
             elUno.setId(1L);
             elUno.setCreationDate(OffsetDateTime.now());
             elUno.setName("Jefe");
             
             EntryLevel elDos=new EntryLevel();
             elDos.setId(2L);
             elDos.setCreationDate(OffsetDateTime.now());
             elDos.setName("Encargado");
             
             EntryLevel elTres=new EntryLevel();
             elTres.setId(3L);
             elTres.setCreationDate(OffsetDateTime.now());
             elTres.setName("Operador");
             
             elRepo.save(elUno);
             elRepo.save(elDos);
             elRepo.save(elTres);
             
         }
         
         
         
         long count3=auRepo.count();
         if(count3==0){
            AppUser au=new AppUser();
            au.setCreationDate(OffsetDateTime.now());
            au.setUserName("Admin");
            au.setPassword("Admin");
            au.setEntryLevel(elRepo.getOne(1L));
            au.setId(1L);
            
            auRepo.save(au);
         }
               
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
