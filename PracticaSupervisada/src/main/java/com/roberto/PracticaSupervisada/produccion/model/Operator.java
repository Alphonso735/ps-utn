/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import com.roberto.PracticaSupervisada.produccion.model.deserializer.DateDeserializer;
import java.util.Date;
import javax.persistence.DiscriminatorValue;

/**
 *
 * @author roberto
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@DiscriminatorValue("O")
public class Operator extends BaseEntity implements Serializable {
    private String name;
    private String address;
    
    
   @JsonDeserialize(using = DateDeserializer.class)
    private Date birthDate;
    
    @JsonIgnore
    @OneToMany(mappedBy = "operator")
    private Set<ProductionOrder> lstProductionOrder;
    
    
}
