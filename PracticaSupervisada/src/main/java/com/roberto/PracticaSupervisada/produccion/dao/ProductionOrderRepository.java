/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.dao;

import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberto
 */
@Repository
public interface ProductionOrderRepository extends JpaRepository<ProductionOrder, Long>{

    public List<ProductionOrder> findByDeleteDateNull();

    public List<ProductionOrder> findByStatusId(Long statusId);

    public List<ProductionOrder> findByOperatorIdAndDeleteDateNull(Long idOperator);

    public List<ProductionOrder> findByOperatorIdAndStatusIdAndDeleteDateNull(Long idOperator, long l);

    public List<ProductionOrder> findByMouldIdAndStatusIdAndDeleteDateNull(Long idMould, long l);

    public List<ProductionOrder> findByMachineIdAndStatusIdAndDeleteDateNull(Long idMachine, long l);

    public List<ProductionOrder> findByClientIdAndStatusIdAndDeleteDateNull(Long idClient, long l);

    public List<ProductionOrder> findByStatusIdOrderByEditionDateDesc(Long TERMINADO);
    //*******************************************************************
    public List<ProductionOrder> findByStatusIdAndDeleteDateNull(Long status, Pageable pagina);
    
    public List<ProductionOrder> findByStatusIdAndDeleteDateNotNull(Long status, Pageable pagina);
    //*******************************************************************

    public List<ProductionOrder> findByStatusIdAndCreationDateBetweenOrderByCreationDateDesc(Long status, OffsetDateTime A, OffsetDateTime B);

    public List<ProductionOrder> findByStatusIdAndClientIdAndCreationDateBetweenOrderByCreationDateDesc(Long status, Long idClient, OffsetDateTime begin, OffsetDateTime end);




   
       
    
}
