/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.roberto.PracticaSupervisada.produccion.model.deserializer.DateDeserializer;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author roberto
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatesTransfer implements Serializable{
    
   @JsonDeserialize(using = DateDeserializer.class)
    private Date dateA;
   
   @JsonDeserialize(using = DateDeserializer.class)
    private Date dateB;
}
