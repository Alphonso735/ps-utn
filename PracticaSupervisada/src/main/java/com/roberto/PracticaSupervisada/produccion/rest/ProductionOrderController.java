/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.DeliveryBox;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import com.roberto.PracticaSupervisada.produccion.model.StatusPO;
import com.roberto.PracticaSupervisada.produccion.model.dto.DatesTransfer;
import com.roberto.PracticaSupervisada.produccion.service.ProductionOrderService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class ProductionOrderController {
    @Autowired
    private ProductionOrderService poServ;
     
    private static final Long PRODUCCION=1L;
    private static final Long TERMINADO=2L;
    private static final Long CANCELADO=3L;
    private static final Long ENTREGADO=4L;

   
    //-----------------------------------
    @GetMapping("/ProductionOrders/production/{page}/{mount}")
    public ResponseEntity getPOproduction(@PathVariable("page")int page,
            @PathVariable("mount")int mount){
        List<ProductionOrder> list=poServ.getPageProductionOrder(page, mount, PRODUCCION);
        
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @GetMapping("/ProductionOrders/canceled/{page}/{mount}")
    public ResponseEntity getPOcanceled(@PathVariable("page")int page,
            @PathVariable("mount")int mount){
        List<ProductionOrder> list=poServ.getPageProductionOrder(page, mount, CANCELADO);
        
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @GetMapping("/ProductionOrders/finished/{page}/{mount}")
    public ResponseEntity getPOfinished(@PathVariable("page")int page,
            @PathVariable("mount")int mount){
        List<ProductionOrder> list=poServ.getPageProductionOrder(page, mount, TERMINADO);
        
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @GetMapping("/ProductionOrders/delivered/{page}/{mount}")
    public ResponseEntity getPOdelivered(@PathVariable("page")int page,
            @PathVariable("mount")int mount){
        List<ProductionOrder> list=poServ.getPageProductionOrder(page, mount, ENTREGADO);
        
        return new ResponseEntity(list, HttpStatus.OK);
    }
    //----------------------------------
    @GetMapping("/ProductionOrders/canceled")
    public ResponseEntity getPOCanceled(){
        List<ProductionOrder> list=poServ.getPOCanceled();
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    
    //----------------------------------------------------------
    @PostMapping("/ProductionOrders/production/between")
    public ResponseEntity getAllPOProductionBetween(
            @RequestBody DatesTransfer dateTransfer){
        
        Date dateA=dateTransfer.getDateA();
        Date dateB=dateTransfer.getDateB();
      
       List<ProductionOrder> lst=poServ.getAllPOBetween(PRODUCCION,dateA,dateB);
        
        return new ResponseEntity(lst, HttpStatus.OK);
    }
    
    @PostMapping("/ProductionOrders/finished/between")
    public ResponseEntity getAllPOFinishedBetween(
        @RequestBody DatesTransfer datesTransfer){
        
        List<ProductionOrder> lst=poServ.getAllPOBetween(TERMINADO,datesTransfer.getDateA(),
                datesTransfer.getDateB());
        
        return new ResponseEntity(lst, HttpStatus.OK);
    }
    
    @PostMapping("/ProductionOrders/canceled/between")
    public ResponseEntity getAllPOCanceledBetween(
        @RequestBody DatesTransfer datesTransfer){
      
        List<ProductionOrder> lst=poServ.getAllPOBetween(CANCELADO,datesTransfer.getDateA(),
                datesTransfer.getDateB());
        
        return new ResponseEntity(lst, HttpStatus.OK);
    }
    @PostMapping("/ProductionOrders/delivered/between")
     public ResponseEntity getAllPODeliveredBetween(
        @RequestBody DatesTransfer datesTransfer){
        
        List<ProductionOrder> lst=poServ.getAllPOBetween(ENTREGADO,datesTransfer.getDateA(),
                datesTransfer.getDateB());
        
        return new ResponseEntity(lst, HttpStatus.OK);
     }
    
    //--------------------------------------------------------------
   
    @GetMapping("/ProductionOrders/{idOP}/DeliveryBox")
    public ResponseEntity getAllDeliveryBoxFrom(@PathVariable("idOP")Long idOP){
        List<DeliveryBox> list;
        if(idOP != null){
            list=poServ.getAllDeliveryBoxFrom(idOP);
            return new ResponseEntity(list, HttpStatus.OK);
          }
        return new ResponseEntity(null, HttpStatus.NOT_FOUND);     
    }
    
    @PostMapping("/ProductionOrders/{idOP}/DeliveryBox")
    public ResponseEntity saveBoxToProductionOrder(@RequestBody DeliveryBox deliveryBox,
            @PathVariable("idOP") Long idOP){
        DeliveryBox result=poServ.saveBoxToProductionOrder(deliveryBox,idOP);
        
        if(result==null)
            return new ResponseEntity(result, HttpStatus.NOT_FOUND);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }
    
    @DeleteMapping("/ProductionOrders/{idOP}/DeliveryBox/{idBox}")
    public ResponseEntity deleteDeliveryBoxFrom(@PathVariable("idOP")Long idOP,
            @PathVariable("idBox")Long idBox){
        Boolean result=poServ.deleteDeliveryBoxFrom(idOP,idBox);
         HttpStatus status; 
         if(result!=null){
             if(result){
                 status=HttpStatus.OK;
             }
             else{
                 status=HttpStatus.NOT_FOUND;
             }
         }else{
             status=HttpStatus.BAD_REQUEST;
         }
          return new ResponseEntity(status);
    }
    
    @GetMapping("/ProductionOrders/{idOP}/NumOfPiecesInBoxes")
    public ResponseEntity getNumOfPiecesInBoxes(@PathVariable("idOP")Long idOP){
        int NumOfPieces=poServ.getNumOfPiecesInBoxes(idOP);
        HttpStatus status;
        if(NumOfPieces!=0)
            status=HttpStatus.OK;
        else
            status=HttpStatus.NOT_FOUND;
        
        return new ResponseEntity(NumOfPieces, status);
    }
    
    @PostMapping("/ProductionOrders/getNumPiecesArray")
    public ResponseEntity getNumPiecesArray(@RequestBody ArrayList<Long> listIdPO){
        List<Integer> result=poServ.getNumPiecesArray(listIdPO);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }
    //-----------------------------------------------------
    
     @GetMapping("/ProductionOrders")
    public ResponseEntity getAllProductionOrders(){
        List<ProductionOrder> list=poServ.getAllProductionOrders();
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @PostMapping("/ProductionOrders")
    public ResponseEntity saveProductionOrder(@RequestBody ProductionOrder productionOrder){
        Boolean po=poServ.saveProductionOrder(productionOrder);
        
        HttpStatus estado;   
        if(po!=null){
            if(po){
                estado=HttpStatus.OK;
            }else{
                estado=HttpStatus.INSUFFICIENT_STORAGE;
            }
        }else{
            estado=HttpStatus.BAD_REQUEST;
        }
            return new ResponseEntity(estado);
    }
    
    @PutMapping("/ProductionOrders")
    public ResponseEntity updateProductionOrder(@RequestBody ProductionOrder productionOrder){
        Boolean po=poServ.updateProductionOrder(productionOrder);
         HttpStatus estado;   
        if(po!=null){
            if(po){
                estado=HttpStatus.OK;
            }else{
                estado=HttpStatus.INSUFFICIENT_STORAGE;
            }
        }else{
            estado=HttpStatus.BAD_REQUEST;
        }
            return new ResponseEntity(estado);
    }
    
    @DeleteMapping("/ProductionOrders/{idProductionOrder}")
    public ResponseEntity deleteProductionOrder(
            @PathVariable("idProductionOrder")Long idProductionOrder){
        Boolean result=poServ.deleteProductionOrder(idProductionOrder);
        HttpStatus status;
        if(result!=null){
            if(result){
                status=HttpStatus.OK;
            }else{
                status=HttpStatus.NOT_FOUND;
            }
        }else{
            status=HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity(status);
    }
    //---------------------------------------------------------
    //get all the status production order
    @GetMapping("/ProductionOrders/status")
    public ResponseEntity getAllStatus(){
        List<StatusPO> list=poServ.getAllStatus();
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
     //*******************************************************
    private HttpStatus evalStatus(ProductionOrder productionOrder){
        HttpStatus result;
        if(productionOrder!=null)
            result=HttpStatus.OK;
        else
            result=HttpStatus.NOT_FOUND;
        return result;
    }
}
