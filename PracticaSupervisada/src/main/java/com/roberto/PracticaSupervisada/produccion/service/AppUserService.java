/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.AppUserRepository;

import com.roberto.PracticaSupervisada.produccion.model.AppUser;
import java.time.OffsetDateTime;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class AppUserService {
    @Autowired
    private AppUserRepository auRepo;
    //------------------------------

    public AppUser getUserApp(AppUser appUser) {
       AppUser au=auRepo.findByUserNameAndPassword(appUser.getUserName(),appUser.getPassword());
            
       if(au==null)
           return null;
       
        
        return au;
    }

    public Boolean saveAppUser(AppUser appUser) {
       appUser.setCreationDate(OffsetDateTime.now());
       if(auRepo.save(appUser)==null)
           return false;
       return true;
    }

    public Boolean updateAppUser(AppUser appUser) {
        AppUser old=auRepo.getOne(appUser.getId());
        if(old==null)
            return false;
        
        appUser.setCreationDate(old.getCreationDate());
        appUser.setEditionDate(OffsetDateTime.now());
        
        if(auRepo.save(appUser)==null)
            return false;
        
        return true;
    }

    public Boolean deleteAppUser(Long idAU) {
        AppUser old=auRepo.getOne(idAU);
        if(old==null)
            return false;
        
        old.setDeleteDate(OffsetDateTime.now());
        if(auRepo.save(old)==null)
            return false;
        
        return true;
    }

    public List<AppUser> getAllAppUsers() {
        return auRepo.findByDeleteDateNull();
    }
      
}
