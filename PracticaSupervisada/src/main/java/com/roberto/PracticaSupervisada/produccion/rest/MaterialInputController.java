/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.MaterialInput;
import com.roberto.PracticaSupervisada.produccion.service.MaterialInputService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class MaterialInputController {
    @Autowired
    private MaterialInputService matInServ;
    //-----------------------
    
    @PostMapping("/MaterialInput/Add/{idMaterial}/{numBags}")
    public ResponseEntity addMaterialInput(@PathVariable("idMaterial")Long idMaterial,
            @PathVariable("numBags")int numBags){
        
        Boolean result=matInServ.addMaterialInput(idMaterial, numBags);
        
       HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
     @PostMapping("/MaterialInput/Reduce/{idMaterial}/{numBags}")
    public ResponseEntity reduceMaterialInput(@PathVariable("idMaterial")Long idMaterial,
            @PathVariable("numBags")int numBags){
        
        Boolean result=matInServ.reduceMaterialInput(idMaterial, numBags);
        
        HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @GetMapping("/MaterialInput")
    public ResponseEntity getAllMaterialInput(){
        List<MaterialInput> list=matInServ.getAllMaterialInput();
        
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @GetMapping("/MaterialInput/{page}/{mount}")
    public ResponseEntity getMaterialInput(@PathVariable("page")int page,@PathVariable("mount")int mount){
        List<MaterialInput> list=matInServ.getMaterialInput(page,mount);
        
        return new ResponseEntity(list, HttpStatus.OK);
    }
    //*******************************************************
    private HttpStatus evalStatus(MaterialInput material){
        HttpStatus result;
        if(material!=null)
            result=HttpStatus.OK;
        else
            result=HttpStatus.NOT_FOUND;
        return result;
    }
}//endController
