/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.ClientRepository;
import com.roberto.PracticaSupervisada.produccion.dao.ProductionOrderRepository;
import com.roberto.PracticaSupervisada.produccion.model.Client;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class ClientService {
    @Autowired
    private ClientRepository clientRepo;
    @Autowired
    private ProductionOrderRepository poRepo;
    //-------------------------------

    public Boolean saveClient(Client client) {
        client.setCreationDate(OffsetDateTime.now());
        if(clientRepo.save(client)==null)
            return false;
        return true;
    }

    public List<Client> getAllClients() {
        return clientRepo.findByDeleteDateNull();
    }

    public Boolean updateClient(Client client) {
        Client old=clientRepo.getOne(client.getId());
        //-----------------------------
        client.setCreationDate(old.getCreationDate());
        
        client.setEditionDate(OffsetDateTime.now());
         if(clientRepo.save(client)==null)
            return false;
        return true;
    }

    public Boolean deleteClient(Long idClient) {
        Client client=clientRepo.getOne(idClient);
        if(client == null)
            return false;
        
        List<ProductionOrder>list=poRepo.findByClientIdAndStatusIdAndDeleteDateNull(idClient,1L);
        if(!list.isEmpty())
            return false;
        
        client.setDeleteDate(OffsetDateTime.now());
        
       if(clientRepo.save(client)==null)
            return false;
        return true;
    }

    boolean exitsClient(Long id) {
        if(clientRepo.getOne(id)!=null)
            return true;
        return false;
    }

    public List<Client> getClients(int page, int mount) {
        Pageable pagina=PageRequest.of(page, mount, Sort.by("creationDate").descending());
        List<Client> result=clientRepo.findByDeleteDateNull(pagina);
        
        return result;
    }
    
    
}
