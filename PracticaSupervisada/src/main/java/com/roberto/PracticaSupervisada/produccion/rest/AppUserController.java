/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.AppUser;
import com.roberto.PracticaSupervisada.produccion.service.AppUserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class AppUserController {
    @Autowired
    private AppUserService auService;
    //----------------------------
    
    @PostMapping("/AppUsers")
    public ResponseEntity existAppUser(@RequestBody AppUser appUser){
        AppUser au=auService.getUserApp(appUser);
        System.out.println("Hola antes del return");
        return new ResponseEntity(au, evalStatus(au));
    }
    //*******************************************************
    @GetMapping("/AppUsers")
    public ResponseEntity getAllAppUsers(){
        List<AppUser> list=auService.getAllAppUsers();
        return new ResponseEntity(list, HttpStatus.OK);
    }
    @PostMapping("/AppUsers/add")
    public ResponseEntity saveAppUser(@RequestBody AppUser appUser){
        Boolean result=auService.saveAppUser(appUser);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    @PutMapping("/AppUsers")
    public ResponseEntity updateAppUser(@RequestBody AppUser appUser){
        Boolean result=auService.updateAppUser(appUser);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @DeleteMapping("/AppUser/{idAU}")
    public ResponseEntity deleteAppUser(@PathVariable("idAU")Long idAU){
        Boolean result=auService.deleteAppUser(idAU);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    private HttpStatus evalStatus(AppUser appUser){
        HttpStatus result;
        if(appUser!=null)
            result=HttpStatus.OK;
        else
            result=HttpStatus.NOT_FOUND;
        return result;
    }
}//end
