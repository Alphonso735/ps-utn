/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.dao;

import com.roberto.PracticaSupervisada.produccion.model.DeliveryBox;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberto
 */
@Repository
public interface DeliveryBoxRepository extends JpaRepository<DeliveryBox, Long>{

    public List<DeliveryBox> findByProductionOrderIdAndDeleteDateNull(Long idOP);
    
    @Query(value = "SELECT SUM(num_of_items) FROM delivery_box WHERE production_order_id=?1 AND "
            + "delete_date is null",
            nativeQuery = true)
    public Integer getNumOfPiecesInBoxes(Long idOP);

   
    
}
