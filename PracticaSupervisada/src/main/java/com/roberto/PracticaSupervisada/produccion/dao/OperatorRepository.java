/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.dao;

import com.roberto.PracticaSupervisada.produccion.model.Operator;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberto
 */
@Repository
public interface OperatorRepository extends JpaRepository<Operator, Long>{
    //this method return all operators that have been deleted
    public List<Operator> findByDeleteDateNotNull();
    
    //this method return all operators that have not been deleted
    public List<Operator> findByDeleteDateNull();

    //this method return an operator if it has not been deleted. Searching by his id
    public Operator findByIdAndDeleteDateNull(Long idOperator);
    
}
