/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.Operator;
import com.roberto.PracticaSupervisada.produccion.service.OperatorService;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class OperatorController {
    @Autowired
    private OperatorService operServ;
    
    //--------------------------------------
    
    @PostMapping("/Operators")
    public ResponseEntity saveOperator(@RequestBody Operator operator){
        Operator newOperator=operServ.saveOperator(operator);
        return new ResponseEntity(newOperator, evalStatus(newOperator));
    }
    
    //@CrossOrigin
    @GetMapping("/Operators")
    public ResponseEntity getAllOperators(){
        List<Operator> list=operServ.getAllOperators();
        return new ResponseEntity(list, HttpStatus.OK);
    }
    
    @GetMapping("/Operators/{idOperator}")
    public ResponseEntity getOperator(@PathVariable("idOperator")Long idOperator){
        Operator oper=operServ.getOperator(idOperator);
        return new ResponseEntity(oper, HttpStatus.OK);
    }
    
    @DeleteMapping("/Operators/{idOperator}")
    public ResponseEntity deleteOperator(@PathVariable("idOperator")Long idOperator){
        Boolean result=operServ.deleteOperator(idOperator);
        HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    @PutMapping("/Operators")
    public ResponseEntity updateOperator(@RequestBody Operator operator){
        Boolean result=operServ.updateOperator(operator);
        HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    //*******************************************************
    private HttpStatus evalStatus(Operator operator){
        HttpStatus result;
        if(operator!=null)
            result=HttpStatus.OK;
        else
            result=HttpStatus.NOT_FOUND;
        return result;
    }
}//end
