/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.DeliveryBoxRepository;
import com.roberto.PracticaSupervisada.produccion.model.DeliveryBox;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
class DeliveryBoxService {
    @Autowired
    private DeliveryBoxRepository boxRepo;
    //------------------------------------

    List<DeliveryBox> getAllDeliveryBoxFrom(Long idOP) {
        return boxRepo.findByProductionOrderIdAndDeleteDateNull(idOP);
    }

    DeliveryBox saveBox(DeliveryBox deliveryBox) {
       return boxRepo.save(deliveryBox);
    }

    DeliveryBox getBox(Long idBox) {
        return boxRepo.getOne(idBox);
    }

    boolean deleteAllBoxFrom(Long idProductionOrder) {
       List<DeliveryBox> list=boxRepo.findByProductionOrderIdAndDeleteDateNull(idProductionOrder);
       
       if(!list.isEmpty()){
          for(DeliveryBox b:list){
            b.setDeleteDate(OffsetDateTime.now());
            boxRepo.save(b);
          }
       }
      return true;
    }
    
    public int getNumOfPiecesInBoxes(Long idOP){
        Integer result=boxRepo.getNumOfPiecesInBoxes(idOP);
        if(result==null)
            return 0;
        return result;
    }
    
}//end
