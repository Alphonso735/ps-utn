/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.ProductionOrderRepository;
import com.roberto.PracticaSupervisada.produccion.model.DeliveryBox;
import com.roberto.PracticaSupervisada.produccion.model.Material;
import com.roberto.PracticaSupervisada.produccion.model.Mould;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import com.roberto.PracticaSupervisada.produccion.model.StatusPO;
import com.roberto.PracticaSupervisada.produccion.model.dto.DatesTransfer;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author roberto
 */
@Service
public class ProductionOrderService {

    @Autowired
    private ProductionOrderRepository poRepo;
    @Autowired
    private MachineService machServ;
    @Autowired
    private MouldService mouldServ;
    @Autowired
    private OperatorService operServ;
    @Autowired
    private StatusPOService stServ;
    @Autowired
    private ClientService clientServ;
    @Autowired
    private MaterialService matServ;
    @Autowired
    private StatusPOService statusServ;
    @Autowired
    private DeliveryBoxService boxServ;
    //-------------------------------

    private static final Long PRODUCCION = 1L;
    private static final Long TERMINADO = 2L;
    private static final Long CANCELADO = 3L;
    private static final Long ENTREGADO = 4L;
    //--------------------------------
    public List<ProductionOrder> getAllProductionOrders() {
        return poRepo.findByDeleteDateNull();
    }

    public Boolean saveProductionOrder(ProductionOrder productionOrder) {
        /*System.out.println("Maquina: "+productionOrder.getMachine().getName());
        System.out.println("molde: "+productionOrder.getMould().getName());
        System.out.println("operador: "+productionOrder.getOperator().getName());
        System.out.println("cliente: "+productionOrder.getClient().getName());
         */
        if (machServ.exitsMachine(productionOrder.getMachine().getId())
                && mouldServ.exitsMould(productionOrder.getMould().getId())
                && operServ.exitsOperator(productionOrder.getOperator().getId())
                && clientServ.exitsClient(productionOrder.getClient().getId())) {

            Material matToUse = productionOrder.getMould().getMaterial();
            float cantMatExist = matServ.getNumOfKilOfMaterial(matToUse.getId());

            float cantMatRequest = productionOrder.cantOfKil();

            if (cantMatRequest <= cantMatExist) {
                productionOrder.setCreationDate(OffsetDateTime.now());

                //Aca le asigno el estado de PRODUCCION.
                productionOrder.setStatus(stServ.getStatus(this.PRODUCCION));

                //----------Redusco la cant de material y guardo
                if (!matServ.reduceMaterial(matToUse.getId(), cantMatRequest)
                        || poRepo.save(productionOrder) == null) {
                    return null;
                }

                return true;//se guardo en forma exitosa   
            } else {
                return false;//no habia suficiente cantidad de material existente
            }

        }

        return null;//no existia algun dato de la orden
    }

    public Boolean updateProductionOrder(ProductionOrder productionOrder) {
        if (machServ.exitsMachine(productionOrder.getMachine().getId())
                && mouldServ.exitsMould(productionOrder.getMould().getId())
                && operServ.exitsOperator(productionOrder.getOperator().getId())
                && clientServ.exitsClient(productionOrder.getClient().getId())) {
            //--------control-----------
           // if(productionOrder.getStatus().getId()==CANCELADO)
             //   return null;
            //-------------------------
            ProductionOrder old = poRepo.getOne(productionOrder.getId());//old po
            Material mat = productionOrder.getMould().getMaterial();

            float oldMatMount = old.cantOfKil();
            float newMatMount = productionOrder.cantOfKil();

            if (oldMatMount != newMatMount) {
                if (oldMatMount > newMatMount) {
                    float diff = oldMatMount - newMatMount;
                    if (!matServ.addMaterial(mat.getId(), diff)) {
                        return false;
                    }
                } else {
                    float diff = newMatMount - oldMatMount;
                    if (!matServ.reduceMaterial(mat.getId(), diff)) {
                        return false;
                    }
                }
            }
            //-----------------------------------------
            productionOrder.setCreationDate(old.getCreationDate());
            productionOrder.setEditionDate(OffsetDateTime.now());
            if (poRepo.save(productionOrder) == null) {
                return false;
            }
            return true;
        }
        //Return null becouse the data is worng   
        return null;

    }

    private boolean existProductionOrder(Long idProductionOrder) {
        if (poRepo.getOne(idProductionOrder) != null) {
            return true;
        }

        return false;
    }
    @Transactional
    public Boolean deleteProductionOrder(Long idProductionOrder) {
        ProductionOrder po = poRepo.getOne(idProductionOrder);
        if (po == null) {
            return null;
        }
        float cantMat=po.cantOfKil();
        Material mat=po.getMould().getMaterial();
        
        if(!matServ.addMaterial(mat.getId(), cantMat))
            return null;
        
        po.setDeleteDate(OffsetDateTime.now());

        StatusPO st = stServ.getStatus(3L);
        if (st == null) {
            return null;
        }

        po.setStatus(st);

        //trato de borrar las cajas de la orden
        if (!boxServ.deleteAllBoxFrom(idProductionOrder)) {
            return false;
        }

        if (poRepo.save(po) == null) {
            return false;
        }

        return true;
    }
    
       
    public List<ProductionOrder> getPageProductionOrder(int page,int mount, Long status){
        
        Pageable pagina=PageRequest.of(page,mount,Sort.by("creationDate").descending());
        List<ProductionOrder> result=null;
        
        if(status== CANCELADO)    
            result=poRepo.findByStatusIdAndDeleteDateNotNull(status,pagina);
        else
            result=poRepo.findByStatusIdAndDeleteDateNull(status,pagina);
            
        return result;
    }
   

    public List<ProductionOrder> getPOCanceled() {
        List<ProductionOrder> lst = poRepo.findByStatusId(CANCELADO);
        return lst;
    }

    public List<StatusPO> getAllStatus() {
        return statusServ.getAllStatusPO();
    }

    public List<DeliveryBox> getAllDeliveryBoxFrom(Long idOP) {
        return boxServ.getAllDeliveryBoxFrom(idOP);
    }

    public DeliveryBox saveBoxToProductionOrder(DeliveryBox deliveryBox, Long idOP) {
        ProductionOrder po = poRepo.getOne(idOP);
        if (po == null) {
            return null;
        }

        deliveryBox.setCreationDate(OffsetDateTime.now());
        deliveryBox.setProductionOrder(po);

        return boxServ.saveBox(deliveryBox);
    }

    public Boolean deleteDeliveryBoxFrom(Long idOP, Long idBox) {
        ProductionOrder po = poRepo.getOne(idOP);
        DeliveryBox db = boxServ.getBox(idBox);
        if (po == null || db == null) {
            return null;
        }

        db.setDeleteDate(OffsetDateTime.now());
        if (boxServ.saveBox(db) != null) {
            return true;
        } else {
            return false;
        }
    }

    public List<ProductionOrder> getAllPOBetween(Long status, Date dateA, Date dateB) {
        OffsetDateTime A = getODT(dateA);
        OffsetDateTime B = getODT(dateB);

        OffsetDateTime begin = A.minusHours(A.getHour()).minusMinutes(A.getMinute());
        OffsetDateTime end = B.plusHours(23L - B.getHour()).plusMinutes(59L - B.getMinute());

        //--------------------------
       
        return poRepo.findByStatusIdAndCreationDateBetweenOrderByCreationDateDesc(status, begin, end);
    }

    //***************************************
    private OffsetDateTime getODT(Date date) {
        return OffsetDateTime.ofInstant(date.toInstant(), ZoneOffset.UTC);
    }

    public int getNumOfPiecesInBoxes(Long idOP) {
        int result=boxServ.getNumOfPiecesInBoxes(idOP);
        return result;
    }

    public List<Integer> getNumPiecesArray(ArrayList<Long> listIdPO) {
        List<Integer>  result=new ArrayList<>();
        //-----------------------------
        for(Long i:listIdPO){
            result.add(boxServ.getNumOfPiecesInBoxes(i));
        }
        //-----------------------------
        return result;
    }
//------------------------------------------------------------------------------
    public List<ProductionOrder> getAllPOBetweenFor(Long status, DatesTransfer dt, Long idClient) {
        OffsetDateTime A = getODT(dt.getDateA());
        OffsetDateTime B = getODT(dt.getDateB());
        
        OffsetDateTime begin = A.minusHours(A.getHour()).minusMinutes(A.getMinute());
        OffsetDateTime end = B.plusHours(23L - B.getHour()).plusMinutes(59L - B.getMinute());
        //-----------------
        return poRepo.findByStatusIdAndClientIdAndCreationDateBetweenOrderByCreationDateDesc(status,idClient,begin,end);
    }

}
