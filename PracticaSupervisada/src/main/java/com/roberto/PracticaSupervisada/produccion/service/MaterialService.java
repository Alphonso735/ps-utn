/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.MaterialRepository;
import com.roberto.PracticaSupervisada.produccion.dao.MouldRepository;
import com.roberto.PracticaSupervisada.produccion.dao.ProductionOrderRepository;
import com.roberto.PracticaSupervisada.produccion.model.Material;
import com.roberto.PracticaSupervisada.produccion.model.Mould;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class MaterialService {

    @Autowired
    private MaterialRepository materialRepo;
    @Autowired
    private MouldRepository moRepo;
    //------------------------------

    //get all materials
    public List<Material> getAllMaterial() {
        return materialRepo.findByDeleteDateNull();
    }

    //get material
    public Material getMaterial(Long idMaterial) {
        return materialRepo.getOne(idMaterial);
    }

    //reduce material
    public boolean reduceMaterial(Long idMaterial, float num) {
        Material material = this.getMaterial(idMaterial);
        
        float stockMaterial = material.getNumOfKil();
        
        if (num <= stockMaterial) {
            material.setNumOfKil(stockMaterial - num);
            
            material.setEditionDate(OffsetDateTime.now());
            
            materialRepo.save(material);
            
            return true;
        }
        return false;
    }

    //add material
    public boolean addMaterial(Long idMaterial, float num) {
        Material material = this.getMaterial(idMaterial);
        float stockMaterial = material.getNumOfKil();
        material.setNumOfKil(num+stockMaterial);
        material.setEditionDate(OffsetDateTime.now());
        materialRepo.save(material);
        return true;
    }
    
    //save material
    public Boolean saveMaterial(Material material){
        material.setCreationDate(OffsetDateTime.now());
        Material newMat=materialRepo.save(material);
        
        if(newMat!=null)
            return true;
        return false;
    }
    
    //delete material
    public Boolean deleteMaterial(Long idMaterial){
        Material material=materialRepo.getOne(idMaterial);
        if(material==null)
            return false;
        
        List<Mould>list=moRepo.findByMaterialIdAndDeleteDateNull(idMaterial);
        if(!list.isEmpty())
            return false;
        
        material.setDeleteDate(OffsetDateTime.now());
        materialRepo.save(material);
       
        return true;      
    }
    
    //updateMaterial
    public Boolean updateMaterial(Material material){
        Material mat=materialRepo.getOne(material.getId());
        if(mat==null){
            return false;
        }
        material.setCreationDate(mat.getCreationDate());
        material.setEditionDate(OffsetDateTime.now());
        materialRepo.save(material);
        return true;
    }
    //get the numOfKil of a material
    public float getNumOfKilOfMaterial(Long idMaterial){
        Material mat=materialRepo.getOne(idMaterial);
        
        if(mat==null)
            return 0f;
        
        return mat.getNumOfKil();
    }
}//endservice

