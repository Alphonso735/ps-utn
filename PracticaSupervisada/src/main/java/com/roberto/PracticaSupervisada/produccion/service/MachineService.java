/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.MachineRepository;
import com.roberto.PracticaSupervisada.produccion.dao.ProductionOrderRepository;
import com.roberto.PracticaSupervisada.produccion.model.Machine;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class MachineService {
    @Autowired
    private MachineRepository machineRepo;
    @Autowired
    private ProductionOrderRepository poRepo;
    //------------------------
    //get all the machines
    public List<Machine> getAllMachines(){
        return machineRepo.findByDeleteDateNull();
    }
    //save a machine
    public Boolean saveMachine(Machine machine){
        machine.setCreationDate(OffsetDateTime.now());
        Machine newMachine=machineRepo.save(machine);
        
        if(newMachine==null)
            return false;
        return true;
    }
    
    //delete a machine
    public Boolean deleteMachine(Long idMachine){
        Machine machine=machineRepo.getOne(idMachine);
        
        if(machine==null)
            return false;
        
        List<ProductionOrder>list=poRepo.findByMachineIdAndStatusIdAndDeleteDateNull(idMachine,1L);
       if(!list.isEmpty())
           return false;
       
        machine.setDeleteDate(OffsetDateTime.now());
        if(machineRepo.save(machine)==null)
            return false;
        
        return true;
    }
    
    //get a machine
    public Machine getMachine(Long idMachine){
        Machine result=machineRepo.findByIdAndDeleteDateNull(idMachine);
        if(result==null)
            return null;
           
        return result; 
    }

    public Boolean updateMachine(Machine machine) {
        if(machine!=null){
            Machine old=machineRepo.getOne(machine.getId());
            if(old!=null){
                machine.setCreationDate(old.getCreationDate());
                machine.setEditionDate(OffsetDateTime.now());
                machineRepo.save(machine);
                return true;
            }
        }
        return false;
    }
    
    public boolean exitsMachine(Long idMachine){
        Machine machine=machineRepo.getOne(idMachine);
        if(machine!=null)
            return true;
        return false;
    }
    
    
}//endService
