/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.dao;

import com.roberto.PracticaSupervisada.produccion.model.Mould;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberto
 */
@Repository
public interface MouldRepository extends JpaRepository<Mould, Long> {

    public List<Mould> findByDeleteDateNull();

    public Mould findByIdAndDeleteDateNull(Long idMould);

    public List<Mould> findByMaterialIdAndDeleteDateNull(Long idMaterial);

    public List<Mould> findByDeleteDateNull(Pageable pagina);
    
}
