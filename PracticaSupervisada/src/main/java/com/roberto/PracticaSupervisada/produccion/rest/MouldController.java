/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.Mould;
import com.roberto.PracticaSupervisada.produccion.service.MouldService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class MouldController {

    @Autowired
    private MouldService mouldServ;
    //----------------------------------------

    @GetMapping("/Moulds")
    public ResponseEntity getAllMoulds() {
        List<Mould> lstMould = mouldServ.getAllMoulds();
        return new ResponseEntity(lstMould, HttpStatus.OK);
    }
    
    @GetMapping("/Moulds/{page}/{mount}")
    public ResponseEntity getMoulds(@PathVariable("page")int page,@PathVariable("mount")int mount){
        List<Mould> result=mouldServ.getMoulds(page,mount);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }    

    @GetMapping("/Moulds/{idMould}")
    public ResponseEntity getAMould(@PathVariable("idMould") Long idMould) {

        Mould mould = mouldServ.getMould(idMould);
        HttpStatus result;

        if (mould != null) {
            result = HttpStatus.OK;
        } else {
            result = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity(mould, result);
    }

    @PostMapping("/Moulds")
    public ResponseEntity saveMould(@RequestBody Mould mould) {
       Boolean result= mouldServ.saveMould(mould);
      HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @DeleteMapping("/Moulds/{idMould}")
    public ResponseEntity deleteMould(@PathVariable("idMould")Long idMould){
        Boolean result=mouldServ.deleteMould(idMould);
        HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @PutMapping("/Moulds")
    public ResponseEntity updateMould(@RequestBody Mould mould){
        Boolean result=mouldServ.updateMould(mould);
       HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    //*******************************************************
    private HttpStatus evalStatus(Mould mould){
        HttpStatus result;
        if(mould!=null)
            result=HttpStatus.OK;
        else
            result=HttpStatus.NOT_FOUND;
        return result;
    }
}//endController
