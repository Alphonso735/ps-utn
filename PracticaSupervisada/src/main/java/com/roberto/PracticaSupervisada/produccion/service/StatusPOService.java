/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.StatusPORepository;
import com.roberto.PracticaSupervisada.produccion.model.StatusPO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class StatusPOService {
    @Autowired
    private StatusPORepository stRepo;
    //------------------------
    public List<StatusPO> getAllStatusPO(){
        return stRepo.findByDeleteDateNull();
    }

    public StatusPO getStatus(Long iStatus) {
        StatusPO st=stRepo.getOne(iStatus);
        if(st==null)
            return null;
        return st;
    }
    
}
