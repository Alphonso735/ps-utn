/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.roberto.PracticaSupervisada.produccion.model.deserializer.DateDeserializer;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *
 * @author roberto
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class MaterialInput extends BaseEntity{
    
    private int numOfBags;
    
    @ManyToOne
    private Material material;
    
    @JsonDeserialize(using = DateDeserializer.class)
    private Date date;
    
    private boolean status;
}
