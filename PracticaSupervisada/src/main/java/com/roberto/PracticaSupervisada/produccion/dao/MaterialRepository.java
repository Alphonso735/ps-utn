/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.dao;

import com.roberto.PracticaSupervisada.produccion.model.Material;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author roberto
 */
@Repository
public interface MaterialRepository extends JpaRepository<Material, Long>{

    public List<Material> findByDeleteDateNull();
    
}
