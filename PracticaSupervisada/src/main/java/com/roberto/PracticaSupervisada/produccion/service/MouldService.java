/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.MouldRepository;
import com.roberto.PracticaSupervisada.produccion.dao.ProductionOrderRepository;
import com.roberto.PracticaSupervisada.produccion.model.Mould;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class MouldService {
    @Autowired
    private MouldRepository mouldRepo;
    @Autowired
    private ProductionOrderRepository poRepo;
    //------------------------
    //get all moulds
    public List<Mould> getAllMoulds(){
        return mouldRepo.findByDeleteDateNull();
    }
    
    //save a mould
    public Boolean saveMould(Mould mould){
        mould.setCreationDate(OffsetDateTime.now());
        if(mouldRepo.save(mould)!=null)
            return true;
         
          return false;
    }
    
    public Boolean deleteMould(Long idMould){
        Mould mould=mouldRepo.getOne(idMould);
        
        if(mould==null)
            return false;
        
       List<ProductionOrder> list=poRepo.findByMouldIdAndStatusIdAndDeleteDateNull(idMould,1L);
        if(!list.isEmpty())
            return false;
        
        mould.setDeleteDate(OffsetDateTime.now());
        
        if(mouldRepo.save(mould)==null)
            return false;
        
        return true;
    }
    
    public Mould getMould(Long idMould){
        Mould result=mouldRepo.findByIdAndDeleteDateNull(idMould);
        if(result!=null)
            return result;
        else
            return null;
    }
    
    public Boolean updateMould(Mould mould){
        Mould m=mouldRepo.getOne(mould.getId());
        if(m==null)
            return null;
        mould.setCreationDate(m.getCreationDate());
        mould.setEditionDate(OffsetDateTime.now());
        if( mouldRepo.save(mould)!=null)
            return true;
        return false;
    }
    
    public boolean exitsMould(Long idMould){
        if(mouldRepo.getOne(idMould)!=null)
            return true;
        return false;
    }

    public List<Mould> getMoulds(int page, int mount) {
       Pageable pagina=PageRequest.of(page, mount, Sort.by("creationDate").descending().and(Sort.by("name").ascending()));
       List<Mould> result=mouldRepo.findByDeleteDateNull(pagina);
       
       return result;
    }
}//endService
