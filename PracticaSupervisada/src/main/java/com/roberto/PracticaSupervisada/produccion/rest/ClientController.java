/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.model.Client;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import com.roberto.PracticaSupervisada.produccion.model.dto.DatesTransfer;
import com.roberto.PracticaSupervisada.produccion.service.ClientService;
import com.roberto.PracticaSupervisada.produccion.service.ProductionOrderService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class ClientController {
    @Autowired
    private ClientService clientServ;
    @Autowired
    private ProductionOrderService poServ;
    //---------------------------
    private static final Long PRODUCCION=1L;
    private static final Long TERMINADO=2L;
    private static final Long CANCELADO=3L;
    private static final Long ENTREGADO=4L;
    //---------------------------

    
    @PostMapping("/Clients")
    public ResponseEntity saveClient(@RequestBody Client client){
        Boolean result=clientServ.saveClient(client);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @PutMapping("/Clients")
    public ResponseEntity updateClient(@RequestBody Client client){
         Boolean result=clientServ.updateClient(client);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    @DeleteMapping("/Clients/{idClient}")
    public ResponseEntity deleteClient(@PathVariable("idClient") Long idClient){
        Boolean result=clientServ.deleteClient(idClient);
         HttpStatus status;
         if(result)
         {
             status=HttpStatus.OK;
         }else{
             status=HttpStatus.NOT_FOUND;
         }
         return new ResponseEntity(status);
    }
    
    
    @GetMapping("/Clients")
    public ResponseEntity getAllClients(){
        List<Client> list=clientServ.getAllClients();
        return new ResponseEntity(list, HttpStatus.OK);
    }
     
    @GetMapping("/Clients/{page}/{mount}")
    public ResponseEntity getClients(@PathVariable("page")int page,@PathVariable("mount")int mount){
        List<Client> result=clientServ.getClients(page,mount);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }
    
    //----------------------------------------------------------------
    @PostMapping("/Clients/{idClient}/production/between")
    public ResponseEntity getAllPOProductionBetween(@PathVariable("idClient")Long idClient,
            @RequestBody DatesTransfer dt){
        List<ProductionOrder> result=poServ.getAllPOBetweenFor(PRODUCCION,dt,idClient);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }
    @PostMapping("/Clients/{idClient}/finished/between")
    public ResponseEntity getAllPOFinishedBetween(@PathVariable("idClient")Long idClient,
            @RequestBody DatesTransfer dt){
        List<ProductionOrder> result=poServ.getAllPOBetweenFor(TERMINADO,dt,idClient);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }
    @PostMapping("/Clients/{idClient}/canceled/between")
    public ResponseEntity getAllPOCanceledBetween(@PathVariable("idClient")Long idClient,
            @RequestBody DatesTransfer dt){
        List<ProductionOrder> result=poServ.getAllPOBetweenFor(CANCELADO,dt,idClient);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }
    @PostMapping("/Clients/{idClient}/delivered/between")
    public ResponseEntity getAllPODeliveredBetween(@PathVariable("idClient")Long idClient,
            @RequestBody DatesTransfer dt){
        List<ProductionOrder> result=poServ.getAllPOBetweenFor(ENTREGADO,dt,idClient);
        
        return new ResponseEntity(result, HttpStatus.OK);
    }
}//end
