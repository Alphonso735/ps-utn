/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.service;

import com.roberto.PracticaSupervisada.produccion.dao.OperatorRepository;
import com.roberto.PracticaSupervisada.produccion.dao.ProductionOrderRepository;
import com.roberto.PracticaSupervisada.produccion.model.Operator;
import com.roberto.PracticaSupervisada.produccion.model.ProductionOrder;
import java.time.OffsetDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author roberto
 */
@Service
public class OperatorService {
    @Autowired
    private OperatorRepository operRepo;
    @Autowired
    private ProductionOrderRepository poRepo;
    //--------------------------------------
    
    public List<Operator> getAllOperators(){
       //return operRepo.findByDeleteDateNotNull();
       return operRepo.findByDeleteDateNull();
    }
    
    public Operator saveOperator(Operator operator){
        operator.setCreationDate(OffsetDateTime.now());
        return operRepo.save(operator);
    }

    public Boolean deleteOperator(Long idOperator) {
        Operator oper=operRepo.getOne(idOperator);
        if(oper==null)
            return false;
        
        List<ProductionOrder> lista=poRepo.findByOperatorIdAndStatusIdAndDeleteDateNull(idOperator,1L);
        
        if(!lista.isEmpty())
            return false;
        
        oper.setDeleteDate(OffsetDateTime.now());
        if( operRepo.save(oper)==null)
            return false;
        
        return true;
    }

    public Operator getOperator(Long idOperator) {
        Operator result=operRepo.findByIdAndDeleteDateNull(idOperator);
        if(result!=null)
            return result;
        else
            return null;
    }

    public Boolean updateOperator(Operator operator) {
        Operator old=operRepo.getOne(operator.getId());
        
        if(old== null)
            return false;
        
        operator.setCreationDate(old.getCreationDate());
        operator.setEditionDate(OffsetDateTime.now());
        if(operRepo.save(operator)==null)
            return false;
        
        return true;
    }
    
    public boolean exitsOperator(Long idOperator){
        if(operRepo.getOne(idOperator)!=null)
            return true;
        return false;
    }
}//end
