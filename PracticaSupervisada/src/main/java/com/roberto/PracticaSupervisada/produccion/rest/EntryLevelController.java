/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.rest;

import com.roberto.PracticaSupervisada.produccion.service.EntryLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author roberto
 */
@RestController
public class EntryLevelController {
    @Autowired
    private EntryLevelService elServ;
    //---------------------------
    
    @GetMapping("/EntryLevels")
    public ResponseEntity getAllEntryLevels(){
        return new ResponseEntity(elServ.getAllEntryLevel(), HttpStatus.OK);
    }
}
