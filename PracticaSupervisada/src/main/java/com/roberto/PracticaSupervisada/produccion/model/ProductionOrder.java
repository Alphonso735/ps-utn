/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.roberto.PracticaSupervisada.produccion.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *
 * @author roberto
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ProductionOrder extends BaseEntity implements Serializable {
    
    @ManyToOne
    private Mould mould;
    
    @ManyToOne
    private Operator operator;
    
    @ManyToOne
    private Machine machine;
    
    @ManyToOne
    private StatusPO status;
    
    @ManyToOne
    private Client client;
    
    private int pieceCount;
    
    @JsonIgnore
    @OneToMany(mappedBy = "productionOrder")
    private Set<DeliveryBox> lstDeliveryBox;
    
    public float cantOfKil(){
        float weightInKil=mould.getWeight()/1000;
        float result=this.pieceCount*weightInKil;
        return result;
    }
}
